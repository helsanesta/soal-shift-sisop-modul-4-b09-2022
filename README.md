# soal-shift-sisop-modul-4-B09-2022


## Anggota Kelompok
| **Nama** | **NRP** |
| ------ | ------ |
| Helsa Nesta Dhaifullah | 5025201005 |
| Zunia Aswaroh | 5025201058 |
| Tengku Fredly Reinaldo | 5025201198 |


## SOAL 1

Anya adalah adalah seorang programmer wibu yang suka mengoleksi anime. Dia sangat senang membuat program yang dikolaborasikan dengan anime. Suatu hari, Anya sedang ingin membuat system dengan ketentuan berikut:

### Note

filesystem berfungsi normal layaknya linux pada umumnya, Mount source (root) filesystem adalah directory /home/[USER]/Documents, dalam penamaan file ‘/’ diabaikan, dan ekstensi tidak perlu di-encode

### Poin Soal 1
- Semua direktori dengan awalan “Animeku_” akan terencode dengan ketentuan semua file yang terdapat huruf besar akan ter encode dengan atbash cipher dan huruf kecil akan terencode dengan rot13.

    Contoh : 
    “Animeku_/anya_FORGER.txt” → “Animeku_/naln_ULITVI.txt”

- Semua direktori di-rename dengan awalan “Animeku_”, maka direktori tersebut akan menjadi direktori ter-encode dengan ketentuan sama dengan 1a.

- Apabila direktori yang terenkripsi di-rename menjadi tidak ter-encode, maka isi direktori tersebut akan terdecode.

- Setiap data yang terencode akan masuk dalam file “Wibu.log”

    Contoh isi: 

    RENAME terenkripsi /home/[USER]/Downloads/hai --> /home/[USER]/Downloads/Animeku_hebat 

    RENAME terdecode /home/[USER]/Downloads/Animeku_ --> /home/[USER]/Downloads/Coba

- Metode encode pada suatu direktori juga berlaku terhadap direktori yang ada di dalamnya.(rekursif)

### Solusi Soal 1
- Untuk menyelesaikan decode dan encode, saya menggunakan fungsi seperti di bawah ini
```
void encode(char* strEnc1) { 
	if(strcmp(strEnc1, ".") == 0 || strcmp(strEnc1, "..") == 0)
        return;
    
    int strLength = strlen(strEnc1);
    for(int i = 0; i < strLength; i++) {
		if(strEnc1[i] == '/') 
            continue;
		if(strEnc1[i]=='.')
            break;
        
		if(strEnc1[i]>='A'&&strEnc1[i]<='Z')
            strEnc1[i] = 'Z'+'A'-strEnc1[i];
        if(strEnc1[i]>='a'&&strEnc1[i]<='z')
        {
            int j = (int)strEnc1[i];
            j = j + ROT;
            if(j > 122)
                j = j - 122 + 97 - 1;

            strEnc1[i] = j;            
        }
    }
}

void decode(char * strDec1){ //decrypt encv1_
	if(strcmp(strDec1, ".") == 0 || strcmp(strDec1, "..") == 0 || strstr(strDec1, "/") == NULL) 
        return;
    
    int strLength = strlen(strDec1), s=0;
	for(int i = strLength; i >= 0; i--){
		if(strDec1[i]=='/')break;

		if(strDec1[i]=='.'){//nyari titik terakhir
			strLength = i;
			break;
		}
	}
	for(int i = 0; i < strLength; i++){
		if(strDec1[i]== '/'){
			s = i+1;
			break;
		}
	}
    for(int i = s; i < strLength; i++) {
		if(strDec1[i] =='/'){
            continue;
        }
        if(strDec1[i]>='A'&&strDec1[i]<='Z')
            strDec1[i] = 'Z'+'A'-strDec1[i];
        if(strDec1[i]>='a'&&strDec1[i]<='z')
        {
            int j = (int)strDec1[i];
            j = j + ROT;
            if(j > 122)
                j = j - 122 + 97 - 1;
            strDec1[i] = j;
        }
    }
	
}
```
- Untuk mencatat setiap perubahan nama direktori, digunakan fungsi di bawah ini
```
void createLog(const char* old, char* new) {
	
	FILE * logFile = fopen(PathAnimekuLog, "a");
    fprintf(logFile, "%s --> %s\n", old, new);
    fclose(logFile);
}
```
